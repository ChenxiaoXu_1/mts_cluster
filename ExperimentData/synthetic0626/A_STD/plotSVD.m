clc
clear all
close all

load('A0.mat')
%A = eye(65);
[U,S,V] = svd(A);
subplot(2,2,1)
imagesc(A)
colorbar
subplot(2,2,2)
imagesc(U)
colorbar
subplot(2,2,3)
imagesc(S)
colorbar
subplot(2,2,4)
imagesc(V)
colorbar