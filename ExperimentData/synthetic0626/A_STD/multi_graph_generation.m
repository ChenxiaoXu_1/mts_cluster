clc
clear all
close all

n = 65;

A0 = generate_from_scratch(n, '0');
A1 = generate_additional('1', 0, A0, 5555, 16, 31);
A2 = generate_additional('2', 0, A0, 6666, 31, 45);
A3 = generate_additional('3', 0, A0, 7777, 45, 16);

subplot(2,2,1)
imagesc(A0)
grid on

subplot(2,2,2)
imagesc(A1)
grid on

subplot(2,2,3)
imagesc(A2)
grid on

subplot(2,2,4)
imagesc(A3)
grid on