clc
clear all
close all

load('A1.mat')
%A = eye(65);
A = A';
% for i=1:size(A,1)
%     A(i,i) = 1;
% end

[vector, value] = eig(A);

figure(1)
imagesc(vector)
% figure(2)
% imagesc(value)
figure(3)
imagesc(A)

value = diag(value);
idx = find(value);

figure(2)
plot(value)