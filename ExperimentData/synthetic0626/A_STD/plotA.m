clear all
close all
clc

figure(1)
subplot(2,2,1)
load('A0.mat')
imagesc(A)
axis off
axis equal
title('A0')

subplot(2,2,2)
load('A1.mat')
imagesc(A)
axis off
axis equal
title('A1')

subplot(2,2,3)
load('A2.mat')
imagesc(A)
axis off
axis equal
title('A2')

subplot(2,2,4)
load('A3.mat')
imagesc(A)
axis off
axis equal
title('A3')

