import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.parameter import Parameter
import numpy as np
from sklearn.preprocessing import normalize
import math
from torch.nn.utils import weight_norm
import SingularValue_layer

class LowRankGraph_USV(nn.Module):
    def __init__(self, args, data, hidden):
        super(LowRankGraph_USV, self).__init__()
        self.m = data.m
        self.low_rank = args.low_rank
        self.use_cuda = args.cuda
        self.Q = hidden
        self.graph_batchnorm = args.graph_batchnorm
        self.pre_win_long = args.MaxLen - args.pre_win
        
        self.single_graph_net = []
        
        # self.single_graph_net.append(weight_norm(nn.Linear(self.m, self.low_rank, bias = False)))                
        # self.single_graph_net.append(nn.BatchNorm1d(self.Q*self.pre_win_long))    
        # self.single_graph_net.append(SingularValue_layer.SV_Model(data, self.low_rank, self.use_cuda))       
        # self.single_graph_net.append(weight_norm(nn.Linear(self.low_rank, self.m, bias = False)))                               
        # self.single_graph_net.append(nn.BatchNorm1d(self.Q*self.pre_win_long))
 
        self.single_graph_net.append(weight_norm(nn.Linear(self.m, self.low_rank, bias = False)))                
        self.single_graph_net.append(SingularValue_layer.SV_Model(data, self.low_rank, self.use_cuda))       
        self.single_graph_net.append(weight_norm(nn.Linear(self.low_rank, self.m, bias = False)))                               
 
        if self.graph_batchnorm:
            self.single_graph_net.append(nn.BatchNorm1d(self.Q*self.pre_win_long))    
            self.single_graph_net.append(nn.BatchNorm1d(self.Q*self.pre_win_long))    
            
        self.single_graph_net = nn.ModuleList(self.single_graph_net)         

    def Batch_Norm(self, x_sp, layer_number, batch_size):
        k = x_sp.shape[-1]
        x_sp = x_sp.reshape((batch_size, self.pre_win_long, self.Q, k))
        x_sp = x_sp.reshape((batch_size, self.pre_win_long*self.Q, k))
        x_sp = self.single_graph_net[layer_number](x_sp)
        x_sp = x_sp.reshape((batch_size, self.pre_win_long, self.Q, k))
        x_sp = x_sp.reshape((batch_size*self.pre_win_long, self.Q, k))
       
        return x_sp
        
    def forward(self, x_sp, batch_size):        

        x_sp = self.single_graph_net[0](x_sp)    #(batchsize, Q, k) 
        if self.graph_batchnorm:
            x_sp = self.Batch_Norm(x_sp, 3, batch_size)
        x_sp = F.tanh(x_sp/5.)
        #x_sp = self.dropout(x_sp)   #(batchsize, Q, k)
     
        x_sp = self.single_graph_net[1](x_sp)  #(batchsize, Q, k)    

        x_sp = self.single_graph_net[2](x_sp)  #(batchsize, Q, m)
        if self.graph_batchnorm:
            x_sp = self.Batch_Norm(x_sp, 4, batch_size)
        x_sp = F.tanh(x_sp/5.)
        #x_sp = self.dropout(x_sp)

        return x_sp
    
class LowRankGraph_EF(nn.Module):
    def __init__(self, args, data, hidden):
        super(LowRankGraph_EF, self).__init__()
        self.m = data.m
        self.low_rank = args.low_rank
        self.use_cuda = args.cuda
        self.Q = hidden
        self.pre_win_long = args.pre_win_long
        self.graph_batchnorm = args.graph_batchnorm
        self.self_autoregress = args.self_autoregress
        
        self.single_graph_net = []
        
        self.single_graph_net.append(weight_norm(nn.Linear(self.m, self.low_rank, bias = False)))                
        self.single_graph_net.append(weight_norm(nn.Linear(self.low_rank, self.m, bias = False)))                               
 
        self.single_graph_net = nn.ModuleList(self.single_graph_net)          
        
    def forward(self, x_sp, batch_size):      
        if self.self_autoregress:
            x_sp_tmp = x_sp.clone()
        x_sp = self.single_graph_net[0](x_sp)    #(batchsize, Q, k) 
        x_sp = self.single_graph_net[1](x_sp)    #(batchsize, Q, m)
        x_sp = F.relu(x_sp/1.)

        if self.self_autoregress:
            x_sp = F.relu(x_sp/1.)
            x_sp = x_sp + x_sp_tmp
        
        x_sp = F.relu(x_sp/1.)
        #x_sp = self.dropout(x_sp)

        return x_sp
    
    
    
    
    
    