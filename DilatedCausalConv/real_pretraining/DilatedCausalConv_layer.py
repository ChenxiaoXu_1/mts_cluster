import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.parameter import Parameter
from torch.nn.utils import weight_norm

class DilatedCausalConv(nn.Module):
    def __init__(self, args, dilation_size, in_channel, out_channel):
        super(DilatedCausalConv, self).__init__()
        self.kernel_size = args.kernel_size
        self.padding_size = (self.kernel_size-1) * dilation_size
        self.in_channel = in_channel
        self.out_channel = out_channel
    
        #stide are all set to 1
        self.CausalLayer = weight_norm(nn.Conv1d(self.in_channel, self.out_channel, self.kernel_size, stride=1, padding=self.padding_size, dilation = dilation_size))
        self.CausalLayer.weight.data.normal_(0, 1e-10)        

    def forward(self, x0):
        self.m = x0.shape[1] #(batch, m, channel, hidden_timestamp)
        # x1_list = []
        
        # for variable_i in range(self.m):
        #     x1_i = x0[:,variable_i,:,:] #(batch, channel, hidden_timestamp)       in_channel
        #     x1_i = self.CausalLayer(x1_i)  #(batch, channel, hidden_timestamp)    out_channel
        #     x1_i = x1_i[:,:,:-self.padding_size]
        #     x1_i = F.relu(x1_i)
        #     #x1_i = self.dropout(x1_i)
        #     x1_list.append(x1_i)        
                
        # x1 = torch.stack(x1_list, dim = 1) #(batch, m, channel, hidden_timestamps)
        # return x1
        
        batch_size, self.m = x0.shape[0], x0.shape[1] #(batch, m, channel, hidden_timestamp)
        
        x1 = x0.reshape((batch_size*self.m, self.in_channel, -1)) #(batch_size*m, channel, hidden_timestamp)
 
        x1 = self.CausalLayer(x1)
        x1 = x1[:,:,:-self.padding_size]
        x1 = F.relu(x1.reshape((batch_size, self.m, self.out_channel, -1))) #(batch_size, m, channel, hidden_timestamp)
 
        return x1