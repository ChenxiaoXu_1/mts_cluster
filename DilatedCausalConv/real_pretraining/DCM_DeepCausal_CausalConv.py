import torch
import torch.nn as nn
import torch.nn.functional as F
#from torch.nn.parameter import Parameter
import DilatedCausalConv_layer, FinalRowWise_layer, GraphLearning_layer
import numpy as np


## granger full rank
class Model(nn.Module):
    def __init__(self, args, data):
        super(Model, self).__init__()
        self.use_cuda = args.cuda
        self.m = data.m
        self.kernel_size = args.kernel_size
        self.channel_multiplier = args.channel_multiplier

        self.batch_size = args.batch_size
        self.NumCluster = args.NumCluster
        self.MaxLen = args.MaxLen

        self.Q = args.NumCausalLayer
        
        self.low_rank = args.low_rank
        self.reduce_dim = args.reduce_dim        

        self.pre_win = args.pre_win
        self.pre_win_long = args.pre_win_long
        self.graph_learning = args.graph_learning
        self.self_autoregress = args.self_autoregress

        self.totalQ = 0
        ### Module 1: define CausalLayer
        self.CausalLayerList = []
        for layer_i in range(self.Q):
            dilation_size = 2 ** layer_i
            in_channel = self.channel_multiplier ** layer_i
            out_channel = self.channel_multiplier ** (layer_i+1)
            self.totalQ += out_channel
            self.CausalLayerList.append(DilatedCausalConv_layer.DilatedCausalConv(args, dilation_size, in_channel, out_channel))
        self.CausalLayerList = nn.ModuleList(self.CausalLayerList)
        
        self.graph_net = []
        self.compress_net = []
        
        for cluster_i in range(self.NumCluster):
            ### Module 2           
            if self.graph_learning == 'USV':
                clust_graph_net = GraphLearning_layer.LowRankGraph_USV(args, data, self.totalQ)
            else:
                clust_graph_net = GraphLearning_layer.LowRankGraph_EF(args, data, self.totalQ)
                
            self.graph_net.append(clust_graph_net)
            
            ### Module 3 & 4
            clust_compress_net = []
 
            clust_compress_net.append((nn.Linear(self.totalQ, self.reduce_dim)))            
            clust_compress_net.append(FinalRowWise_layer.FR_Model(args, data))

            clust_compress_net = nn.ModuleList(clust_compress_net)
            self.compress_net.append(clust_compress_net)
                        
        self.graph_net = nn.ModuleList(self.graph_net)
        self.compress_net = nn.ModuleList(self.compress_net)
        self.dropout = nn.Dropout(args.dropout)
                      
    def forward(self, inputs):
        x0 = inputs #(batch_size, m, timestamps) 
        batch_size = x0.shape[0]
        ### Module 1
        z_list = []
        for layer_i in range(self.Q):
            if layer_i == 0:
                x0 = x0.unsqueeze(2) #(batch, m, channel, timestamps)  channel = 1
            x1 = self.CausalLayerList[layer_i](x0)
            z = x1[:,:,:,:].clone() #(batch, m, channel, timestamps)            
            z_list.append(z)
            x0 = x1
        
        ## Module 2    
        # z_p = torch.cat(z_list, dim = 2) #(batch, m, pQ, timestamps) 
        # totalLen = z_p.shape[-1]

        # final_y_list = [[] for i in range(self.NumCluster)]
        
        # for window_i in range(totalLen):
        #     x_p = z_p[:,:,:,window_i]   #(batch, m, pQ) 
            
        #     for cluster_i in range(self.NumCluster):
        #         x_sp = x_p.transpose(2,1).contiguous() #(batchsize, pQ, m)
        #         x_sp = self.graph_net[cluster_i](x_sp)                    
        #         x_sp = x_sp.transpose(2,1).contiguous() #(batchsize, m, pQ)
                
        #         ### Module 3
        #         x_sp = self.compress_net[cluster_i][-2](x_sp) #(batchsize, m, reduce_dim)
        #         x_sp = F.tanh(x_sp/5.)
        #         x_sp = self.dropout(x_sp)
    
        #         ### Module 4                   
        #         final_y = self.compress_net[cluster_i][-1](x_sp) #(batch, pre_win, m)
        #         final_y_list[cluster_i].append(final_y)
            
        # final_y_tensor_list = []
        # for cluster_i in range(self.NumCluster):
        #     tmp_tensor = final_y_list[cluster_i]
        #     final_y_tensor_list.append(torch.stack(tmp_tensor, dim = 1)) #(batch, timestamps, pre_win, m)            
 
        z_p = torch.cat(z_list, dim = 2) #(batchsize, m, pQ, pre_win_long)
        z_p = z_p.permute((0,3,1,2)) #(batchsize, pre_win_long, m, pQ)
        predict_timestamp = z_p.shape[1]
        z_p = z_p.reshape((batch_size*predict_timestamp, self.m, self.totalQ)) #(batch*pre_win_long, m, pQ)
        final_y_tensor_list = []
        
        for cluster_i in range(self.NumCluster):
            x_sp = z_p.transpose(2,1).contiguous() #(batch*pre_win_long, pQ, m)
            x_sp = self.graph_net[cluster_i](x_sp, batch_size)                    
            x_sp = x_sp.transpose(2,1).contiguous() #(batch*pre_win_long, m, pQ)
            
            ### Module 3
            x_sp = self.compress_net[cluster_i][-2](x_sp) #(batch*pre_win_long, m, reduce_dim)
            x_sp = F.tanh(x_sp/5.)
            x_sp = self.dropout(x_sp)

            ### Module 4                   
            final_y = self.compress_net[cluster_i][-1](x_sp) #(batchsize*pre_win_long, pre_win, m)
            final_y = final_y.reshape((batch_size, predict_timestamp, self.pre_win, self.m)) #(batchsize, pre_win_long, pre_win, m)
            final_y_tensor_list.append(final_y)
           
        return final_y_tensor_list  

    def predict_relationship_inside(self):
        CGraph_list = []
        for cluster_i in range(self.NumCluster):
            if self.graph_learning == 'USV':                
                A = self.graph_net[cluster_i].single_graph_net[0].weight.transpose(0,1)
                B = torch.diag(self.graph_net[cluster_i].single_graph_net[1].weight.squeeze())
                C = self.graph_net[cluster_i].single_graph_net[2].weight.transpose(0,1)
                CGraph = torch.abs(torch.matmul(torch.matmul(A,B),C))
            
            elif self.graph_learning == 'EF':                
                A = self.graph_net[cluster_i].single_graph_net[0].weight.transpose(0,1)
                B = self.graph_net[cluster_i].single_graph_net[1].weight.transpose(0,1)
                CGraph = torch.abs(torch.matmul(A,B))

            CGraph[range(self.m), range(self.m)] = 0    
            CGraph_list.append(CGraph)
                   
        return CGraph_list
                   
        return CGraph_list, C
         