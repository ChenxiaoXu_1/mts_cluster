import torch
import numpy as np;
from torch.autograd import Variable
from scipy.io import loadmat
import math
from scipy.stats import norm as ssnorm

def _normalized(Data):
    
    m = Data.shape[1]
    Data_norm = np.zeros(Data.shape)
    
    for i in range(m):
        Mean = np.mean(Data[:,i])
        Std = np.std(Data[:,i])
        Data_norm[:,i] = (Data[:,i] - Mean)/Std
    
    return Data_norm

def _normalized_batch(Data):
    
    #normalize the whole chunk of data
    
    NumExample = len(Data)
    m = Data[0].shape[0]
    Data_norm = []
    
    for j in range(NumExample):
        Data_slice = Data[j]  #(variables, timestamps)
        Data_norm_slice = np.zeros(Data_slice.shape)
        for i in range(m):
            
            Mean = np.mean(Data_slice[i,:])
            Std = np.std(Data_slice[i,:])
            Data_norm_slice[i,:] = (Data_slice[i,:] - Mean)/Std
        
        Data_norm.append(Data_norm_slice)
        
    return Data_norm

def _copula_map(Seri, delta):
    out = np.zeros(Seri.shape)
    T = len(Seri)
    for i in range(T):
        out[i] = np.sum(Seri < Seri[i]) / len(Seri)
        
    out[out < delta] = delta
    out[out > 1-delta] = 1-delta
    
    return out
    
def _copula(Data):
    
    Data = np.transpose(Data)   
        
    n, T = Data.shape[0], Data.shape[1]
    Data_norm = np.zeros(Data.shape)
        
    delta = 1/(4*math.pow(T,1/4)*math.sqrt(math.pi*math.log(T)))
    
    for i in range(n):
        Data_norm[i,:] = _copula_map(Data[i,:], delta)
        
    Data_norm = ssnorm.ppf(Data_norm)
    return np.transpose(Data_norm)

class Data_utility(object):
    def __init__(self, args):
        self.cuda = args.cuda
        self.model = args.model
        
        self.data_name = args.data_name        
        self.random_shuffle = args.random_shuffle
        
        self.copula = args.copula
        self.normalize = args.normalize
        self.data_path = args.data_path
        
        self.load_dataset()
        self.tensor_reform()
        
    def load_dataset(self):    
        self.X = np.load(self.data_path + self.data_name + '_TrainData.npy', allow_pickle=True)   #(instances, variables, timestamps) timestamps not equal
        self.labels = np.load(self.data_path + self.data_name + '_Labels.npy')
    
        self.NumCluster = len(np.unique(self.labels))
        self.TotalExample = len(self.X)
        self.m = self.X[0].shape[0]
        
    def tensor_reform(self):
        self.SeqLen = []
        for example_i in range(self.TotalExample):
            self.SeqLen.append(self.X[example_i].shape[1])
            for variable_i in range(self.m):
                Mean = np.mean(self.X[example_i][variable_i,:])
                Std = np.std(self.X[example_i][variable_i,:])
                self.X[example_i][variable_i,:] = (self.X[example_i][variable_i,:] - Mean) / Std

        self.SeqLen = np.array(self.SeqLen) #(Example, )
        self.MaxLen = max(self.SeqLen)
    
        X_train = np.zeros((self.TotalExample, self.m, self.MaxLen)) #(examples, m, seq_len)
        for example_i in range(self.TotalExample):
            seqLen = self.X[example_i].shape[1]
            X_train[example_i,:,:] = np.pad(self.X[example_i], ((0,0), (0,self.MaxLen-seqLen)), 'constant')
            
        X_valid = X_train.copy()
                
        label_train = self.labels
        label_valid = self.labels.copy()
        
        SeqLen_train = self.SeqLen.copy()
        SeqLen_valid = self.SeqLen.copy()
                
        if self.random_shuffle:
            random_index = np.random.permutation(X_train.shape[0])            
            X_train = X_train[random_index]            
            label_train = label_train[random_index]
            SeqLen_train = SeqLen_train[random_index]
        
        X_train = torch.tensor(X_train, dtype=torch.float)
        X_valid = torch.tensor(X_valid, dtype=torch.float)               
        
        self.train = [X_train, label_train, SeqLen_train]
        self.valid = [X_valid, label_valid, SeqLen_valid]
            
    def get_batches(self, data, batch_size, shuffle = False):
        
        inputs = data[0]
        labels = data[1]
        seqLens = data[2]

        length = len(inputs)
        if shuffle:
            index = torch.randperm(length)
        else:
            index = torch.LongTensor(range(length))
        start_idx = 0
        
        while (start_idx < length):
            end_idx = min(length, start_idx + batch_size)
            excerpt = index[start_idx:end_idx]
            sequence = inputs[excerpt] 
            label = labels[excerpt]
            seqLen = seqLens[excerpt]
            
            if (self.cuda):
                sequence = sequence.cuda()

            data = [sequence, label, seqLen]
            yield data
            start_idx += batch_size