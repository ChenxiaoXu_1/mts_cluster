import torch
import numpy as np;
from torch.autograd import Variable
from scipy.io import loadmat
import math
from scipy.stats import norm as ssnorm

def _normalized(Data):
    
    m = Data.shape[1]
    Data_norm = np.zeros(Data.shape)
    
    for i in range(m):
        Mean = np.mean(Data[:,i])
        Std = np.std(Data[:,i])
        Data_norm[:,i] = (Data[:,i] - Mean)/Std
    
    return Data_norm

def _normalized_batch(Data):
    
    #normalize the whole chunk of data
    
    NumExample = len(Data)
    m = Data[0].shape[0]
    Data_norm = []
    
    for j in range(NumExample):
        Data_slice = Data[j]  #(variables, timestamps)
        Data_norm_slice = np.zeros(Data_slice.shape)
        for i in range(m):            
            Mean = np.mean(Data_slice[i,:])
            Std = np.std(Data_slice[i,:])
            Data_norm_slice[i,:] = (Data_slice[i,:] - Mean)/Std
    
        Data_norm.append(Data_norm_slice)
        
    return Data_norm

def _copula_map(Seri, delta):
    out = np.zeros(Seri.shape)
    T = len(Seri)
    for i in range(T):
        out[i] = np.sum(Seri < Seri[i]) / len(Seri)
        
    out[out < delta] = delta
    out[out > 1-delta] = 1-delta
    
    return out
    
def _copula(Data):
    
    Data = np.transpose(Data)   
        
    n, T = Data.shape[0], Data.shape[1]
    Data_norm = np.zeros(Data.shape)
        
    delta = 1/(4*math.pow(T,1/4)*math.sqrt(math.pi*math.log(T)))
    
    for i in range(n):
        Data_norm[i,:] = _copula_map(Data[i,:], delta)
        
    Data_norm = ssnorm.ppf(Data_norm)
    return np.transpose(Data_norm)

class Data_utility(object):
    def __init__(self, args):
        self.cuda = args.cuda
        self.model = args.model
        
        self.NumCluster = args.NumCluster
        self.SeqLen = args.SeqLen
        self.Example = args.Example
        
        self.random_shuffle = args.random_shuffle
                
        self.pre_win = args.pre_win 

        self.copula = args.copula
        self.normalize = args.normalize

        self.data_path = args.data_path
        
        self.generate_synthetic_set()
        self.tensor_reform()
        
    def generate_synthetic_set(self):
                
        self.X_train_raw_list = []                
        self.label_train_list = []
    
        for clust_i in range(self.NumCluster):
            datafile = self.data_path + 'filter_norm_expression' + str(clust_i) + '.mat'
            rawdat = loadmat(datafile)['expression']
            
            SeqLen = self.SeqLen[clust_i]
            Example = self.Example[clust_i]
            
            start_idx = 100
            X_list = []
            for examp_i in range(Example):
                X_list.append(np.array(rawdat[start_idx:start_idx+SeqLen,:].transpose((1,0))))
                start_idx += SeqLen
            
            X = np.stack(X_list, axis=0) #(Examples, Variables, Timestamps)
            self.X_train_raw_list.append(X) #(cluster, examples, variables, timestamps)    
            self.label_train_list.append(clust_i*np.ones(Example,))
        
        self.m = rawdat.shape[1]
        self.MaxLen = self.X_train_raw_list[0].shape[-1]
        
    def tensor_reform(self):

        X_train_list = []

        for clust_i in range(self.NumCluster):    
            X = self.X_train_raw_list[clust_i]   #(examples, variables, timestamps)

            for example_i in range(self.Example[clust_i]):
                for variable_i in range(self.m):
                    Mean = np.mean(X[example_i,variable_i,:])
                    Std = np.std(X[example_i,variable_i,:])
                    X[example_i,variable_i,:] = (X[example_i,variable_i,:] - Mean) / Std
    
            X_train_list.append(X)
            
        X_train = np.concatenate(X_train_list)   #(Examples*clusters, variables, timestamps)
        X_valid = X_train.copy()

        label_train = np.concatenate(self.label_train_list)
        label_valid = label_train.copy()
        
        if self.random_shuffle:
            random_index = np.random.permutation(X_train.shape[0])
            X_train = X_train[random_index]
            label_train = label_train[random_index]     

        X_train = torch.Tensor(X_train)
        X_valid = torch.Tensor(X_valid)

        self.train = [X_train, label_train]
        self.valid = [X_valid, label_valid]
            
    def get_batches(self, data, batch_size, shuffle = False):
        inputs = data[0]
        labels = data[1]

        length = len(inputs)
        if shuffle:
            index = torch.randperm(length)
        else:
            index = torch.LongTensor(range(length))
        start_idx = 0
        
        while (start_idx < length):
            end_idx = min(length, start_idx + batch_size)
            excerpt = index[start_idx:end_idx]
            sequence = inputs[excerpt] 
            label = labels[excerpt]
           
            if (self.cuda):
                sequence = sequence.cuda()
                
            data = [sequence, label]
            yield data
            start_idx += batch_size