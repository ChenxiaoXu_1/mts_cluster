import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.parameter import Parameter
import numpy as np
from sklearn.preprocessing import normalize
import math
from torch.nn.utils import weight_norm
import SingularValue_layer

class LowRankGraph(nn.Module):
    def __init__(self, args, data, hidden):
        super(LowRankGraph, self).__init__()
        self.m = data.m
        self.low_rank = args.low_rank
        self.use_cuda = args.cuda
        self.Q = hidden
        
        self.single_graph_net = []
        
        self.single_graph_net.append(weight_norm(nn.Linear(self.m, self.low_rank, bias = False)))                
        self.single_graph_net.append(nn.BatchNorm1d(self.Q))    
        self.single_graph_net.append(SingularValue_layer.SV_Model(data, self.low_rank, self.use_cuda))       
        self.single_graph_net.append(weight_norm(nn.Linear(self.low_rank, self.m, bias = False)))                               
        self.single_graph_net.append(nn.BatchNorm1d(self.Q))
 
        self.single_graph_net = nn.ModuleList(self.single_graph_net)          
        
    def forward(self, x_sp):        
        x_sp = self.single_graph_net[0](x_sp)    #(batchsize, Q, k) 
        x_sp = self.single_graph_net[1](x_sp)   #(batchsize, Q, k) 
        x_sp = F.tanh(x_sp/5.)
        #x_sp = self.dropout(x_sp)   #(batchsize, Q, k)
     
        x_sp = self.single_graph_net[2](x_sp)  #(batchsize, Q, k)    

        x_sp = self.single_graph_net[3](x_sp)  #(batchsize, Q, m)
        x_sp = self.single_graph_net[4](x_sp)  #(batchsize, Q, m)
        x_sp = F.tanh(x_sp/5.)
        #x_sp = self.dropout(x_sp)

        return x_sp