import torch
import torch.nn as nn
import torch.nn.functional as F
#from torch.nn.parameter import Parameter
import DilatedCausalConv_layer, FinalRowWise_layer, GraphLearning_layer
import numpy as np


## granger full rank
class Model(nn.Module):
    def __init__(self, args, data):
        super(Model, self).__init__()
        self.use_cuda = args.cuda
        self.m = data.m
        self.kernel_size = args.kernel_size
        self.channel_multiplier = args.channel_multiplier

        self.batch_size = args.batch_size
        self.NumCluster = args.NumCluster
        self.MaxLen = args.MaxLen

        self.Q = args.NumCausalLayer
        self.doubelCausalStack = args.doubelCausalStack
        
        self.low_rank = args.low_rank
        self.reduce_dim = args.reduce_dim        

        self.graph_init = args.graph_init
        self.pre_win = args.pre_win
        self.pre_win_long = args.pre_win_long

        self.totalQ = 0
        ### Module 1: define CausalLayer
        self.CausalLayerList = []
        for layer_i in range(self.Q):
            dilation_size = 2 ** layer_i
            in_channel = self.channel_multiplier ** layer_i
            out_channel = self.channel_multiplier ** (layer_i+1)
            self.totalQ += out_channel
            self.CausalLayerList.append(DilatedCausalConv_layer.DilatedCausalConv(args, dilation_size, in_channel, out_channel))
        self.CausalLayerList = nn.ModuleList(self.CausalLayerList)
        
        self.graph_net = []
        self.compress_net = []
        
        for cluster_i in range(self.NumCluster):
            ### Module 2           
            clust_graph_net = GraphLearning_layer.LowRankGraph(args, data, self.totalQ)
            self.graph_net.append(clust_graph_net)
            
            ### Module 3 & 4
            clust_compress_net = []
 
            clust_compress_net.append((nn.Linear(self.totalQ, self.reduce_dim)))            
            clust_compress_net.append(FinalRowWise_layer.FR_Model(args, data))

            clust_compress_net = nn.ModuleList(clust_compress_net)
            self.compress_net.append(clust_compress_net)
                        
        self.graph_net = nn.ModuleList(self.graph_net)
        self.compress_net = nn.ModuleList(self.compress_net)
        self.dropout = nn.Dropout(args.dropout)
                      
    def forward(self, inputs):
        x_input = inputs.unsqueeze(0) #(1, m, timestamps) (batch_size are alway set to 1)
        ### Module 1
        x0 = x_input        
        z_list = []
        for layer_i in range(self.Q):
            if layer_i == 0:
                x0 = x0.unsqueeze(2) #(batch, m, channel, timestamps)  channel = 1
            x1 = self.CausalLayerList[layer_i](x0)
            z = x1[:,:,:,-self.pre_win_long:].clone() #(batch, m, channel, pre_win_long)            
            z_list.append(z)
            x0 = x1
        
        ### Module 2    
        z_p = torch.cat(z_list, dim = 2) #(batch, m, pQ, pre_win_long) 
        final_y_list = [[] for i in range(self.NumCluster)]
        
        for window_i in range(self.pre_win_long):
            x_p = z_p[:,:,:,window_i]   #(batch, m, pQ) 
            
            for cluster_i in range(self.NumCluster):
                x_sp = x_p.transpose(2,1).contiguous() #(batchsize, pQ, m)
                x_sp = self.graph_net[cluster_i](x_sp)                    
                x_sp = x_sp.transpose(2,1).contiguous() #(batchsize, m, pQ)
                
                ### Module 3
                x_sp = self.compress_net[cluster_i][-2](x_sp) #(batchsize, m, reduce_dim)
                x_sp = F.tanh(x_sp/5.)
                x_sp = self.dropout(x_sp)
    
                ### Module 4                   
                final_y = self.compress_net[cluster_i][-1](x_sp) #(batch, pre_win, m)
                final_y_list[cluster_i].append(final_y)
            
        final_y_tensor_list = []
        for cluster_i in range(self.NumCluster):
            tmp_tensor = final_y_list[cluster_i]
            final_y_tensor_list.append(torch.stack(tmp_tensor, dim = 1)) #(batch, pre_win_long, pre_win, m)            
            
        return final_y_tensor_list  

    def predict_relationship_inside(self):

        CGraph_list = []
        
        for cluster_i in range(self.NumCluster):
            if self.graph_init == 'uniform':
                A = self.graph_net[cluster_i].single_graph_net[0].weight.transpose(0,1)
                B = torch.diag(self.graph_net[cluster_i].single_graph_net[2].weight.squeeze())
                C = self.graph_net[cluster_i].single_graph_net[3].weight.transpose(0,1)
            else:
                A = self.graph_net[cluster_i].U.transpose(0,1)
                B = torch.diag(self.graph_net[cluster_i].S.squeeze())
                C = self.graph_net[cluster_i].V.transpose(0,1)                
                
            CGraph = torch.abs(torch.matmul(torch.matmul(A,B),C))
            CGraph[range(self.m), range(self.m)] = 0    
            CGraph_list.append(CGraph)
                   
        return CGraph_list, C
         