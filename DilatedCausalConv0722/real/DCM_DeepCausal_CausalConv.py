import torch
import torch.nn as nn
import torch.nn.functional as F
#from torch.nn.parameter import Parameter
import DilatedCausalConv_layer, FinalRowWise_layer, GraphLearning_layer
import numpy as np


## granger full rank
class Model(nn.Module):
    def __init__(self, args, data):
        super(Model, self).__init__()
        self.use_cuda = args.cuda
        self.m = data.m
        self.kernel_size = args.kernel_size
        self.channel_multiplier = args.channel_multiplier

        self.batch_size = args.batch_size
        self.NumCluster = args.NumCluster
        self.MaxLen = args.MaxLen

        self.Q = args.NumCausalLayer
        
        self.low_rank = args.low_rank
        self.reduce_dim = args.reduce_dim        

        self.pre_win = args.pre_win
        self.pre_win_long = args.pre_win_long
        self.graph_learning = args.graph_learning
        self.self_autoregress = args.self_autoregress

        self.totalQ = 0
        ### Module 1: define CausalLayer
        self.CausalLayerList = []
        for layer_i in range(self.Q):
            dilation_size = 2 ** layer_i
            in_channel = self.channel_multiplier ** layer_i
            out_channel = self.channel_multiplier ** (layer_i+1)
            self.totalQ += out_channel
            self.CausalLayerList.append(DilatedCausalConv_layer.DilatedCausalConv(args, dilation_size, in_channel, out_channel))
        self.CausalLayerList = nn.ModuleList(self.CausalLayerList)
        
        self.clust_net = []
        for cluster_i in range(self.NumCluster):
            single_clust_net = []
            #Left grpah embedding
            single_clust_net.append(nn.Linear(self.m, self.low_rank, bias=False)) 
            #compress on intervariable nonlinearity    
            single_clust_net.append(nn.Linear(self.totalQ, self.reduce_dim)) 
            #Row-wise product
            single_clust_net.append(FinalRowWise_layer.FR_Model(args, data)) 
            #Right graph embedding
            single_clust_net.append(nn.Linear(self.low_rank, self.m, bias=False))

            single_clust_net = nn.ModuleList(single_clust_net)
            self.clust_net.append(single_clust_net)
                        
        self.clust_net = nn.ModuleList(self.clust_net)
        self.dropout = nn.Dropout(args.dropout)
                      
    def forward(self, inputs):
        x0 = inputs #(batch_size, m, timestamps) 
        batch_size = x0.shape[0]
        ### Module 1
        z_list = []
        for layer_i in range(self.Q):
            if layer_i == 0:
                x0 = x0.unsqueeze(2) #(batch, m, channel, timestamps)  channel = 1
            x1 = self.CausalLayerList[layer_i](x0)
            z = x1[:,:,:,:].clone() #(batch, m, channel, timestamps)            
            z_list.append(z)
            x0 = x1
         
        z_p = torch.cat(z_list, dim = 2) #(batchsize, m, pQ, pre_win_long)
        z_p = z_p.permute((0,3,1,2)) #(batchsize, pre_win_long, m, pQ)
        predict_timestamp = z_p.shape[1]
        z_p = z_p.reshape((batch_size*predict_timestamp, self.m, self.totalQ)) #(batch*pre_win_long, m, pQ)
        final_y_tensor_list = []
        
        for cluster_i in range(self.NumCluster):
            #Left graph learning
            x_sp = z_p.transpose(2,1).contiguous() #(batch*pre_win_long, pQ, m)
            x_sp = self.clust_net[cluster_i][0](x_sp) #(batch*pre_win_long, pQ, k)
            x_sp = F.relu(x_sp)

            #intervariable nonlinearity
            x_sp = x_sp.transpose(2,1).contiguous() #(batch*pre_win_long, k, pQ)
            x_sp = self.clust_net[cluster_i][1](x_sp) #(batch*pre_win_long, k, reduce_dim)
            x_sp = F.tanh(x_sp/5.)
            x_sp = self.dropout(x_sp)
            
            #rowwise product
            x_sp = self.clust_net[cluster_i][2](x_sp) #(batch*pre_win_long, pre_win, k)
            x_sp = F.tanh(x_sp/5.)

            #Right graph learning
            x_sp = self.clust_net[cluster_i][-1](x_sp) #(batch*pre_win_long, pre_win, m)
            final_y = x_sp.reshape((batch_size, predict_timestamp, self.pre_win, self.m)) #(batchsize, pre_win_long, pre_win, m)
            final_y_tensor_list.append(final_y)
           
        return final_y_tensor_list  

    def predict_relationship_inside(self):
        CGraph_list = []
        for cluster_i in range(self.NumCluster):
            if self.graph_learning == 'EF':                
                A = self.clust_net[cluster_i][0].weight.transpose(0,1)
                B = self.clust_net[cluster_i][-1].weight.transpose(0,1)
                CGraph = torch.abs(torch.matmul(A,B))

            CGraph[range(self.m), range(self.m)] = 0    
            CGraph_list.append(CGraph)
                   
        return CGraph_list
         