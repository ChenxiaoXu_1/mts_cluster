import torch
import torch.nn as nn
import torch.nn.functional as F
#from torch.nn.parameter import Parameter
import FinalRowWise_layer0626, SingularValue_layer, Weighted_layer
import numpy as np
#import LowRank_Graph_layer_onegraph

## granger full rank
class Model(nn.Module):
    def __init__(self, args, data):
        super(Model, self).__init__()
        self.use_cuda = args.cuda
        self.m = data.m
        self.w = args.window

        self.batch_size = args.batch_size
        self.NumCluster = args.NumCluster

        self.p_list = args.p_list
        self.lp_list = args.lp_list
        self.low_rank = args.low_rank
        
        self.pre_win = args.pre_win 
        self.reduce_dim = args.reduce_dim
        
        self.p_allsum = np.sum(self.p_list)
        self.len_p_list = len(self.p_list)

        self.graph_init = args.graph_init
        self.graph_batchnorm = args.graph_batchnorm

        ### Module 1   
        self.P = []

        for p_i in np.arange(0,self.len_p_list):
            self.P.append(Weighted_layer.WL_Model())
        self.P = nn.ModuleList(self.P)
        
        self.pre_linears = [(nn.Linear(self.w, self.lp_list[0]))]
        self.shrink = [nn.Linear(self.lp_list[0], self.p_list[0])]   

        if self.len_p_list>1:
            for p_i in np.arange(1, self.len_p_list):
                self.pre_linears.append(nn.Linear(self.lp_list[p_i-1], self.lp_list[p_i]))                
                self.shrink.append(nn.Linear(self.lp_list[p_i], self.p_list[p_i]))

        self.pre_linears = nn.ModuleList(self.pre_linears)                        
        self.shrink = nn.ModuleList(self.shrink)

        ### Module 2
        self.graph_net = []
        self.compress_net = []
        
        for cluster_i in range(self.NumCluster):
            if self.graph_init == 'uniform':
                clust_graph_net = []
    
                clust_graph_net.append(nn.utils.weight_norm(nn.Linear(self.m, self.low_rank, bias = False))) #m->k                
                clust_graph_net.append(nn.BatchNorm1d(self.p_allsum))    
                clust_graph_net.append(SingularValue_layer.SV_Model(data, self.low_rank, self.use_cuda)) #k->k       
                clust_graph_net.append(nn.utils.weight_norm(nn.Linear(self.low_rank, self.m, bias = False))) #m->m, supervised                               
                clust_graph_net.append(nn.BatchNorm1d(self.p_allsum))
     
                clust_graph_net = nn.ModuleList(clust_graph_net)    
                self.graph_net.append(clust_graph_net)
            else:
                self.graph_net.append(graph_init_GRU.Graph_Model(self.m, self.low_rank, self.graph_init, self.hidden_dim, self.graph_batchnorm))
                
            ### Module 3 & 4
            clust_compress_net = []
 
            clust_compress_net.append((nn.Linear(self.p_allsum, self.reduce_dim)))            
            clust_compress_net.append(FinalRowWise_layer0626.FR_Model(args, data))

            clust_compress_net = nn.ModuleList(clust_compress_net)
            self.compress_net.append(clust_compress_net)
                        
        self.graph_net = nn.ModuleList(self.graph_net)
        self.compress_net = nn.ModuleList(self.compress_net)
        self.dropout = nn.Dropout(args.dropout)
                      
    def forward(self, inputs, seg_len):
        x_input = inputs[0] #(batchsize, segmentation, window, m)
        batch_size = x_input.shape[0]
        MaxSeg = x_input.shape[1]

        # Module 1                
        x = x_input.permute(1,0,3,2) #(segmentation, batchsize, m, window)        
        x = x.reshape((MaxSeg, batch_size*self.m, self.w)).contiguous() #(segmentation, batchsize*m, window)        
        x = self.dropout(x)          
        
        x_org = x
        x_lp = []

        if self.lp_list[0]> self.w:
            padding = nn.ConstantPad2d((0, self.lp_list[0]-self.w, 0, 0), 0)
            x_0n = padding(x_org)
        
        x_0 = x_org

        for layer_i in range(self.len_p_list):  
            x_i = self.pre_linears[layer_i](x_0)
            x_i = F.relu(self.P[layer_i](x_i) + x_0n) #(segmentation, batchsize*m, lp)
            x_0n = x_i
            x_0 = x_i
            x_lp.append(x_i)
            
        x_p = []

        for layer_i in range(self.len_p_list):
            x_i = self.shrink[layer_i](x_lp[layer_i])
            x_i = F.relu(x_i) #(segmentation, batchsize*m, p)
            x_p.append(x_i)
                    
        x_p = torch.cat(x_p, dim = 2) #(segmentation, batchsize*m, pQ)
        
        pQ = x_p.shape[-1]
        
        final_y_list = []
        
        # Module 2    
        for cluster_i in range(self.NumCluster):
            x_sp = x_p.reshape((MaxSeg, batch_size, self.m, pQ)).contiguous() #(segmentation, batchsize, m, pQ)
            x_sp = x_sp.permute((1,0,3,2)).contiguous() #(batchsize, segmentation, pQ, m)
            x_sp = x_sp.reshape((batch_size*MaxSeg, pQ, self.m)) #(batchsize*seg, pQ, m)
            
            if self.graph_init == 'uniform':                
                x_sp = self.graph_net[cluster_i][0](x_sp)    #(batchsize*seg, pQ, k) 
                x_sp = self.graph_net[cluster_i][1](x_sp)   #(batchsize*seg, pQ, k) 
                x_sp = F.tanh(x_sp/5.)
                #x_sp = self.dropout(x_sp)   #(batchsize, hidden_m, k)
              
                x_sp = self.graph_net[cluster_i][2](x_sp)  #(batchsize*seg, pQ, k)    
        
                x_sp = self.graph_net[cluster_i][3](x_sp)  #(batchsize*seg, pQ, m)
                x_sp = self.graph_net[cluster_i][4](x_sp)  #(batchsize*seg, pQ, m)
                x_sp = F.tanh(x_sp/5.)
                #x_sp = self.dropout(x_sp)
            else:
                x_sp = self.graph_net[cluster_i](x_p)
                
            x_sp = x_sp.transpose(2,1).contiguous() #(batchsize*seg, m, PQ)
            
        ## Module 3
            x_sp = self.compress_net[cluster_i][-2](x_sp) #(batchsize*seg, m, reduce_dim)
            x_sp = F.tanh(x_sp/5.)
            x_sp = self.dropout(x_sp)

        ## Module 4                   
            final_y = self.compress_net[cluster_i][-1](x_sp) #(batchsize*seg, prewin, m)
            final_y_list.append(final_y.reshape(batch_size, MaxSeg, self.pre_win, self.m))
            
        return final_y_list  

    def predict_relationship_inside(self):

        CGraph_list = []
        
        for cluster_i in range(self.NumCluster):
            if self.graph_init == 'uniform':
                A = self.graph_net[cluster_i][0].weight.transpose(0,1)
                B = torch.diag(self.graph_net[cluster_i][2].weight.squeeze())
                C = self.graph_net[cluster_i][3].weight.transpose(0,1)
            else:
                A = self.graph_net[cluster_i].U.transpose(0,1)
                B = torch.diag(self.graph_net[cluster_i].S.squeeze())
                C = self.graph_net[cluster_i].V.transpose(0,1)                
                
            CGraph = torch.abs(torch.matmul(torch.matmul(A,B),C))
            CGraph[range(self.m), range(self.m)] = 0    
            CGraph_list.append(CGraph)
                   
        return CGraph_list, C
         