import torch
import numpy as np;
from torch.autograd import Variable
from scipy.io import loadmat
import math
from scipy.stats import norm as ssnorm

def _normalized(Data):
    
    m = Data.shape[1]
    Data_norm = np.zeros(Data.shape)
    
    for i in range(m):
        Mean = np.mean(Data[:,i])
        Std = np.std(Data[:,i])
        Data_norm[:,i] = (Data[:,i] - Mean)/Std
    
    return Data_norm

def _normalized_batch(Data):
    
    #normalize the whole chunk of data
    
    Example = Data.shape[0]
    m = Data.shape[2]
    Data_norm = np.zeros(Data.shape)
    
    for j in range(Example):
        for i in range(m):
            Mean = np.mean(Data[j,:,i])
            Std = np.std(Data[j,:,i])
            Data_norm[j,:,i] = (Data[j,:,i] - Mean)/Std
        
    return Data_norm

def _copula_map(Seri, delta):
    out = np.zeros(Seri.shape)
    T = len(Seri)
    for i in range(T):
        out[i] = np.sum(Seri < Seri[i]) / len(Seri)
        
    out[out < delta] = delta
    out[out > 1-delta] = 1-delta
    
    return out
    
def _copula(Data):
    
    Data = np.transpose(Data)   
        
    n, T = Data.shape[0], Data.shape[1]
    Data_norm = np.zeros(Data.shape)
        
    delta = 1/(4*math.pow(T,1/4)*math.sqrt(math.pi*math.log(T)))
    
    for i in range(n):
        Data_norm[i,:] = _copula_map(Data[i,:], delta)
        
    Data_norm = ssnorm.ppf(Data_norm)
    return np.transpose(Data_norm)

class Data_utility(object):
    def __init__(self, args):
        self.cuda = args.cuda
        self.model = args.model
        
        self.NumCluster = args.NumCluster
        self.SeqLen = args.SeqLen
        self.Example = args.Example
        
        self.P = args.window
        self.h = args.horizon
        self.random_shuffle = args.random_shuffle
                
        self.pre_win = args.pre_win 

        self.copula = args.copula
        self.normalize = args.normalize
        self.train = args.train

        self.data_path = args.data_path
        
        self.generate_synthetic_set()
        self.tensor_reform()
        
    def generate_synthetic_set(self):
                
        self.X_train_raw_list = []                
        self.label_train_list = []
    
        for clust_i in range(self.NumCluster):
            datafile = self.data_path + 'filter_norm_expression' + str(clust_i) + '.mat'
            rawdat = loadmat(datafile)['expression']
            
            SeqLen = self.SeqLen[clust_i]
            Example = self.Example[clust_i]
            
            start_idx = 100
            X_list = []
    
            for examp_i in range(Example):
                X_list.append( np.array(rawdat[start_idx:start_idx+SeqLen,:]) )
                start_idx += SeqLen
    
            X_list = np.stack(X_list) #(examples, timestamps, variables)
            
            self.X_train_raw_list.append(X_list) #(cluster, examples, timestamps, variables)    
            self.label_train_list.append(clust_i*np.ones(self.X_train_raw_list[0].shape[0],))
        
        self.m = rawdat.shape[1]
        
    def tensor_reform(self):

        X_train_list = []
        Y_train_list = []

        for clust_i in range(self.NumCluster):
            
            X_train_raw = self.X_train_raw_list[clust_i]   #(examples, timestamps, variables)
            
            if self.normalize:
                X_train_raw = _normalized_batch(X_train_raw)
                
            X_train_raw = X_train_raw.transpose((1,0,2))    #(timestamps, examples, variables)

            train_set = range(self.P+self.h-1, len(X_train_raw))
            
            TrainTensor = self._batchify_DCM(X_train_raw, train_set)
            
            X_train = TrainTensor[0].permute(2,0,1,3) #(examples, segmentation, window, variables)            
            Y_train = TrainTensor[1].permute(2,0,1,3)
            
            X_train_list.append(X_train)            
            Y_train_list.append(Y_train)

        X_train = torch.cat(X_train_list, dim = 0)   #(examples*clusters, segmentation, prewin, variables)
        Y_train = torch.cat(Y_train_list, dim = 0)
        label_train = np.concatenate(self.label_train_list)
        
        X_valid = X_train.clone()
        Y_valid = Y_train.clone()
        label_valid = label_train.copy()
        
        if self.random_shuffle:
            random_index = torch.randperm(X_train.shape[0])
            
            X_train = X_train[random_index,:,:]
            Y_train = Y_train[random_index,:,:]
            label_train = label_train[random_index.detach().numpy()]     

        self.train = [X_train, Y_train, label_train]
        self.valid = [X_valid, Y_valid, label_valid]
            
    def _batchify_DCM(self, dat, idx_set):
#        horizon = self.h
        n = len(idx_set)
                
        X_list = []
        Y_list = []
        
        i = 0
        while( i < n-self.pre_win+1 ):    
            end = idx_set[i]
            start = end - self.P
            
            X_list.append(torch.from_numpy(dat[start:end,:,:]))
            Y_list.append(torch.from_numpy(dat[end:end+self.pre_win, :,:]))
    #        i += self.P
            i+=1
            
        X = torch.stack(X_list, dim = 0).float()
        Y = torch.stack(Y_list, dim = 0).float() #(segmentation, window, examples, variables)
        
        return [X, Y] #(segmentation, window, examples, variables)

    def get_batches(self, data, batch_size, shuffle = False):
        inputs = data[0]
        targets = data[1]
        labels = data[2]

        length = len(inputs)
        if shuffle:
            index = torch.randperm(length)
        else:
            index = torch.LongTensor(range(length))
        start_idx = 0
        
        while (start_idx < length):
            end_idx = min(length, start_idx + batch_size)
            excerpt = index[start_idx:end_idx]
            X = inputs[excerpt] 
            Y = targets[excerpt]
            label = labels[excerpt]
           
            if (self.cuda):
                X = X.cuda()
                Y = Y.cuda()

            data = [[Variable(X)], Variable(Y), label]
            yield data
            start_idx += batch_size