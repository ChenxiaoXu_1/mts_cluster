import torch
import numpy as np;
from torch.autograd import Variable
from scipy.io import loadmat
import math
from scipy.stats import norm as ssnorm

def _normalized(Data):
    
    m = Data.shape[1]
    Data_norm = np.zeros(Data.shape)
    
    for i in range(m):
        Mean = np.mean(Data[:,i])
        Std = np.std(Data[:,i])
        Data_norm[:,i] = (Data[:,i] - Mean)/Std
    
    return Data_norm

def _normalized_batch(Data):
    
    #normalize the whole chunk of data
    
    NumExample = len(Data)
    m = Data[0].shape[0]
    Data_norm = []
    
    for j in range(NumExample):
        Data_slice = Data[j]  #(variables, timestamps)
        Data_norm_slice = np.zeros(Data_slice.shape)
        for i in range(m):
            
            Mean = np.mean(Data_slice[i,:])
            Std = np.std(Data_slice[i,:])
            Data_norm_slice[i,:] = (Data_slice[i,:] - Mean)/Std
        
        Data_norm.append(Data_norm_slice)
        
    return Data_norm

def _copula_map(Seri, delta):
    out = np.zeros(Seri.shape)
    T = len(Seri)
    for i in range(T):
        out[i] = np.sum(Seri < Seri[i]) / len(Seri)
        
    out[out < delta] = delta
    out[out > 1-delta] = 1-delta
    
    return out
    
def _copula(Data):
    
    Data = np.transpose(Data)   
        
    n, T = Data.shape[0], Data.shape[1]
    Data_norm = np.zeros(Data.shape)
        
    delta = 1/(4*math.pow(T,1/4)*math.sqrt(math.pi*math.log(T)))
    
    for i in range(n):
        Data_norm[i,:] = _copula_map(Data[i,:], delta)
        
    Data_norm = ssnorm.ppf(Data_norm)
    return np.transpose(Data_norm)

class Data_utility(object):
    def __init__(self, args):
        self.cuda = args.cuda
        self.model = args.model
        
        self.data_name = args.data_name
        
        self.P = args.window
        self.h = args.horizon
        self.random_shuffle = args.random_shuffle
                
        self.pre_win = args.pre_win 

        self.copula = args.copula
        self.normalize = args.normalize
        self.train = args.train

        self.data_path = args.data_path
        
        self.load_dataset()
        self.tensor_reform()
        
        
    def load_dataset(self):    
        self.rawdat = np.load(self.data_path + self.data_name + '_TrainData.npy', allow_pickle=True).tolist()
        self.labels = np.load(self.data_path + self.data_name + '_Labels.npy')
    
        self.NumCluster = max(self.labels) - min(self.labels) + 1
        self.TotalExample = len(self.rawdat)
        self.m = self.rawdat[0].shape[0]
        
    def tensor_reform(self):
                
        if self.normalize:
            dat = _normalized_batch(self.rawdat)

        [X, Y, Seg_len] = self._batchify_DCM(dat)
    
      #  TotalExample = len(X)
        X_train = X
        Y_train = Y
        Seg_len_train = Seg_len.copy()
        
#        X_valid = X[int(self.train*TotalExample):,:,:]
#        Y_valid = Y[int(self.train*TotalExample):,:,:] #(examples, segmentation, window, variables)
#        
#        Seg_len_valid = Seg_len[int(self.train*TotalExample):]         
        
        X_valid = X.clone()
        Y_valid = Y.clone() #(examples, segmentation, window, variables)
        
        Seg_len_valid = Seg_len.copy()         
        
        label_train = self.labels
        label_valid = self.labels.copy()
#        label_valid = self.labels[int(self.train*TotalExample):]
        
        if self.random_shuffle:
            random_index = torch.randperm(X_train.shape[0])
            
            X_train = X_train[random_index,:,:]
            Y_train = Y_train[random_index,:,:]
            
            label_train = label_train[random_index.detach().numpy()]
            Seg_len_train = Seg_len_train[random_index.detach().numpy()]
            

        self.train = [X_train, Y_train, label_train, Seg_len_train]
        self.valid = [X_valid, Y_valid, label_valid, Seg_len_valid]
            
    def _batchify_DCM(self, dat):

#        horizon = self.h                  
        X_list = []
        Y_list = []
        Seg_list = []

        MaxSeg = 0
        
        for example_i in range(len(dat)):
            
            X_example_list = []
            Y_example_list = []
            
            example = dat[example_i].transpose(1,0) #(timestamps, variables)
            idx_set = range(self.P+self.h-1, example.shape[0])           
            n = len(idx_set)
        
            i = 0

            while(i < n-self.pre_win+1):
                end = idx_set[i]
                start = end - self.P
                
                X_example_list.append(torch.from_numpy(example[start:end,:]))
                Y_example_list.append(torch.from_numpy(example[end:end+self.pre_win,:]))
    
                i+=1

            MaxSeg = max(MaxSeg, len(X_example_list))
            
            X_example = torch.stack(X_example_list, dim = 0).float()  #(segmentation, window, variables)
            Y_example = torch.stack(Y_example_list, dim = 0).float()
            
            X_list.append(X_example)
            Y_list.append(Y_example)
        
        for example_i in range(len(dat)):
            X_example = X_list[example_i]
            Y_example = Y_list[example_i]

            offset = MaxSeg - len(X_example)
            X_offset = torch.zeros((offset, X_example.shape[1], X_example.shape[2]))
            Y_offset = torch.zeros((offset, Y_example.shape[1], Y_example.shape[2]))
            
            X_list[example_i] = torch.cat((X_example, X_offset), axis = 0) 
            Y_list[example_i] = torch.cat((Y_example, Y_offset), axis = 0) 
            
            Seg_list.append(len(X_example))

        X = torch.stack(X_list, dim = 0).float()
        Y = torch.stack(Y_list, dim = 0).float() #(example, segmentation, window, variables)
        Seg_len = np.array(Seg_list)
        return [X, Y, Seg_len] #(examples, segmentation, window, variables)

    def get_batches(self, data, batch_size, shuffle = False):
        
        inputs = data[0]
        targets = data[1]
        labels = data[2]
        segs = data[3]

        length = len(inputs)
        if shuffle:
            index = torch.randperm(length)
        else:
            index = torch.LongTensor(range(length))
        start_idx = 0
        
        while (start_idx < length):
            end_idx = min(length, start_idx + batch_size)
            excerpt = index[start_idx:end_idx]
            X = inputs[excerpt] 
            Y = targets[excerpt]
            label = labels[excerpt]
            seg = segs[excerpt]
            
            if (self.cuda):
                X = X.cuda()
                Y = Y.cuda()

            data = [[Variable(X)], Variable(Y), label, seg]
            yield data
            start_idx += batch_size